package com.niko.zooindex.util;

/**
 * is a utility class converting Strings to desired grammar for use in frontend
 */
public class StringConversions {

    public static String properGrammar(String beforeConversion){

        String beforeConversionToLower = beforeConversion.toLowerCase();
        String firstLetter = beforeConversionToLower.substring(0, 1).toUpperCase();
        String otherLetters = beforeConversionToLower.substring(1);
        String fullString = firstLetter + otherLetters;

        return fullString;
    }
}
