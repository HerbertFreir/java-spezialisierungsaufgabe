package com.niko.zooindex.enumeration;

/**
 * used to reduce redundant requestmapping-code. For example: if a user wishes to filter employees by salaries, either
 * by exact, less than or greater than a certain value. In order to prevent having to write 3 different requestmappings,
 * the frontend will also send a String representing one of this class' ENUMs to idenfity the intent of the user.
 */
public enum NumberComparator {
    MORE,
    LESS,
    EQUALS;

    /**
     * used for converting a given String into a corresponding ENUM.
     *
     * @param numberComparator
     * @return NumberComparator (ENUM)
     */
    public static NumberComparator convertStringToEnum(String numberComparator){

        String numberComparatorUpperString = numberComparator.toUpperCase();
        NumberComparator numberComparatorConv = NumberComparator.valueOf(numberComparatorUpperString);

        return numberComparatorConv;
    }
}
