package com.niko.zooindex.dto;

/**
 * used for frontend communication
 */
public class SalaryDTO {
    private String salary;
    private String numberComparator;

    /**
     * constructor for filtering employees by their salary.
     *
     * @param salary
     * @param numberComparator
     */
    public SalaryDTO(String salary, String numberComparator) {
        this.salary = salary;
        this.numberComparator = numberComparator;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getNumberComparator() { return numberComparator; }

    public void setNumberComparator(String numberComparator) {
        this.numberComparator = numberComparator;
    }
}
