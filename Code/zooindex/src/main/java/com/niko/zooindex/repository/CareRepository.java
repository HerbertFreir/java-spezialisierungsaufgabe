package com.niko.zooindex.repository;

import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.model.EmployeeEntity;
import com.niko.zooindex.util.ExceptionThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * an extension of {@link CareCRUDRepository} implementing custom functions
 */
@Repository
@Transactional
public class CareRepository {

    @Autowired
    AnimalCRUDRepository acr;
    @Autowired
    EmployeeCRUDRepository ecr;
    @Autowired
    CareCRUDRepository ccr;

    /**
     * adds a new {@link CareEntity} to the database and the hashSets of the required employee and animal
     *
     * @param employeeId
     * @param animalId
     */
    public void setCare(Integer employeeId, Integer animalId){

        Optional<EmployeeEntity> optionalEmployee = ecr.findById(employeeId);
        if(optionalEmployee.isEmpty()){ throw ExceptionThrows.employeeNotFound; }
        Optional<AnimalEntity> optionalAnimal = acr.findById(animalId);
        if(optionalAnimal.isEmpty()){ throw ExceptionThrows.animalNotFound; }

        CareEntity care = new CareEntity(ecr.findById(employeeId).get(), acr.findById(animalId).get());

        care.getAnimal().addCare(care);
        care.getEmployee().addCare(care);
        ccr.save(care);
        ecr.save(care.getEmployee());
        acr.save(care.getAnimal());
    }
}