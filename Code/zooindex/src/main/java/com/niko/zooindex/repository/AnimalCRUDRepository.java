package com.niko.zooindex.repository;

import com.niko.zooindex.enumeration.AnimalGender;
import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.ClassificationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalCRUDRepository extends CrudRepository<AnimalEntity, Integer> {
    AnimalEntity findOneByName (String name);
    List<AnimalEntity> findByNameLike (String name);
    List<AnimalEntity> findByFood (String food);
    List<AnimalEntity> findByAge (int age);
    List<AnimalEntity> findByAgeGreaterThan (int age);
    List<AnimalEntity> findByAgeLessThan (int age);
    List<AnimalEntity> findByGender (AnimalGender animalGender);
    List<AnimalEntity> findByClassification (ClassificationEntity classification);
}
