package com.niko.zooindex.dto;

import com.niko.zooindex.model.ClassificationEntity;
import com.niko.zooindex.util.StringConversions;

/**
 * used for frontend communication
 */
public class AnimalDTO {

    private int id;
    private String name;
    private String food;
    private int age;
    private String gender;
    private String nameLatin;
    private String description;
    private String speciesType;
    private String animalType;

    /**
     * used for frontend communication. AnimalDTO provides full information about any given animal to display in the
     * main list of animals in the zoo. A version with less information to display is {@link NewAnimalDTO}.
     *
     * @param id
     * @param name
     * @param food
     * @param age
     * @param gender
     * @param classification
     */
    public AnimalDTO(int id, String name, String food, int age, String gender, ClassificationEntity classification) {
        this.id = id;
        this.name = name;
        this.food = food;
        this.age = age;
        this.gender = gender;
        this.nameLatin = classification.getAnimalGenus() + " " + classification.getAnimalSpecies();
        this.description = classification.getAnimalDescription();
        this.animalType = classification.getAnimalCommonName();
        this.speciesType = StringConversions.properGrammar(classification.getAnimalClass().toString());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNameLatin() {
        return nameLatin;
    }

    public void setNameLatin(String nameLatin) {
        this.nameLatin = nameLatin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpeciesType() {
        return speciesType;
    }

    public void setSpeciesType(String speciesType) {
        this.speciesType = speciesType;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }
}
