package com.niko.zooindex;

import com.niko.zooindex.enumeration.*;
import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.ClassificationEntity;
import com.niko.zooindex.model.EmployeeEntity;
import com.niko.zooindex.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZooIndexApplication implements CommandLineRunner {

	@Autowired
	AnimalCRUDRepository acr;
	@Autowired
	EmployeeCRUDRepository ecr;
	@Autowired
	AnimalClassificationCRUDRepository accr;
	@Autowired
	CareRepository cr;

	public static void main(String[] args) {
		SpringApplication.run(ZooIndexApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		ClassificationEntity norwegianForestCat = new ClassificationEntity("Norwegische_Waldkatze","Felis",  "catus", "Lebt im Wald und mag Bäume (insbesondere Fichten)", AnimalClass.MAMMAL);
		ClassificationEntity greyWolf = new ClassificationEntity("Grauer_Wolf", "Canis", "Lupus", "Der graue Wolf ist heimisch in Eurasien, Amerika und Afrika", AnimalClass.MAMMAL);
		accr.save(norwegianForestCat);
		accr.save(greyWolf);

		AnimalEntity a1 = new AnimalEntity( "bobby", "catfood", 5, AnimalGender.FEMALE, norwegianForestCat);
		AnimalEntity a2 = new AnimalEntity( "dobby", "catfood", 2, AnimalGender.MALE, norwegianForestCat);
		AnimalEntity a3 = new AnimalEntity( "lobby", "catfood", 4, AnimalGender.FEMALE, norwegianForestCat);
		AnimalEntity a4 = new AnimalEntity( "hobby", "catfood", 8, AnimalGender.MALE, norwegianForestCat);
		AnimalEntity a5 = new AnimalEntity( "knobby", "meat", 12, AnimalGender.FEMALE, greyWolf);
		AnimalEntity a6 = new AnimalEntity( "snobby", "meat", 2, AnimalGender.MALE, greyWolf);
		AnimalEntity a7 = new AnimalEntity( "clobby", "meat", 7, AnimalGender.FEMALE, greyWolf);
		acr.save(a1);
		acr.save(a2);
		acr.save(a3);
		acr.save(a4);
		acr.save(a5);
		acr.save(a6);
		acr.save(a7);

		EmployeeEntity e1 = new EmployeeEntity("Hanna", 1234512345L, "hanna.kgb@mail.ru", 5000, EmploymentType.ADMINISTRATOR, EmployeeGender.FEMALE);
		EmployeeEntity e2 = new EmployeeEntity("Anna", 8467865344L, "anna.kgb@mail.ru", 24600.42, EmploymentType.ANIMAL_EXPERT, EmployeeGender.FEMALE);
		EmployeeEntity e3 = new EmployeeEntity("Banna", 1467894273L, "banna.kgb@mail.ru", 68924.42, EmploymentType.GARDENER, EmployeeGender.FEMALE);
		EmployeeEntity e4 = new EmployeeEntity("Lana", 4646758566L, "Lana.kgb@mail.ru", 35673.42, EmploymentType.GUIDE, EmployeeGender.MALE);
		EmployeeEntity e5 = new EmployeeEntity("Maria", 3352526623L, "maria.nsa@mail.gov", 35673.42, EmploymentType.CLEANER, EmployeeGender.FEMALE);
		EmployeeEntity e6 = new EmployeeEntity("Thalia", 1234060595L, "thalia@thalia.at", 87355.42, EmploymentType.TECHNICIAN, EmployeeGender.MALE);
		EmployeeEntity e7 = new EmployeeEntity("Ivan Ivanovich Ivanovsky", 7234060595L, "ivanivanivan.nkvd@mail.ru", 43673.2, EmploymentType.HANDLER, EmployeeGender.MALE);

		e1.addWorkday(Workday.MONDAY); e1.addWorkday(Workday.TUESDAY); e1.addWorkday(Workday.WEDNESDAY); e1.addWorkday(Workday.THURSDAY);
		e2.addWorkday(Workday.MONDAY); e2.addWorkday(Workday.TUESDAY); e2.addWorkday(Workday.WEDNESDAY); e2.addWorkday(Workday.THURSDAY);
		e3.addWorkday(Workday.MONDAY); e3.addWorkday(Workday.TUESDAY); e3.addWorkday(Workday.WEDNESDAY); e3.addWorkday(Workday.THURSDAY);
		e4.addWorkday(Workday.MONDAY); e4.addWorkday(Workday.WEDNESDAY); e4.addWorkday(Workday.THURSDAY); e4.addWorkday(Workday.THURSDAY);
		e5.addWorkday(Workday.MONDAY); e5.addWorkday(Workday.WEDNESDAY); e5.addWorkday(Workday.THURSDAY); e5.addWorkday(Workday.SATURDAY);
		e6.addWorkday(Workday.MONDAY); e6.addWorkday(Workday.WEDNESDAY); e6.addWorkday(Workday.FRIDAY); e6.addWorkday(Workday.SATURDAY);
		e7.addWorkday(Workday.MONDAY); e7.addWorkday(Workday.THURSDAY); e7.addWorkday(Workday.FRIDAY); e7.addWorkday(Workday.SUNDAY);

		ecr.save(e1);
		ecr.save(e2);
		ecr.save(e3);
		ecr.save(e4);
		ecr.save(e5);
		ecr.save(e6);
		ecr.save(e7);


		cr.setCare(e1.getId(), a1.getId());
		cr.setCare(e1.getId(), a2.getId());
		cr.setCare(e2.getId(), a1.getId());
		cr.setCare(e3.getId(), a2.getId());
		/*CareEntity c1 = new CareEntity(e1, a1);
		CareEntity c2 = new CareEntity(e2, a1);
		CareEntity c3 = new CareEntity(e3, a1);
		CareEntity c4 = new CareEntity(e1, a2);
		CareEntity c5 = new CareEntity(e1, a3);
		CareEntity c6 = new CareEntity(e2, a2);
		CareEntity c7 = new CareEntity(e1, a4);
		CareEntity c8 = new CareEntity(e3, a2);
		careCRUDRepository.save(c1);
		careCRUDRepository.save(c2);
		careCRUDRepository.save(c3);
		careCRUDRepository.save(c4);
		careCRUDRepository.save(c5);
		careCRUDRepository.save(c6);
		careCRUDRepository.save(c7);
		careCRUDRepository.save(c8);*/
	}
}
