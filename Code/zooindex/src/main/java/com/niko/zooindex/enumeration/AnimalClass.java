package com.niko.zooindex.enumeration;

/**
 * represents all types of animals biologists use to categorize lifeforms. It includes "OTHER" in case an addition to
 * the zoo is not of any listed animal class. Such examples include fungi.
 */
public enum AnimalClass {
    MAMMAL,
    BIRD,
    FISH,
    REPTILE,
    AMPHIBIAN,
    THREE_LEG_PAIRS,
    MORE_LEG_PAIRS,
    WORM_LIKE,
    NOT_WORM_LIKE,
    OTHER
}
