package com.niko.zooindex.dto;

import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.util.StringConversions;

/**
 * for frontend communication. This class is used for showing which employees are responsible for any given animal.
 */
public class EmployeeCareDTO {
    private String name;
    private String svnr;
    private String email;
    private String salary;
    private String employment;
    private String gender;

    /**
     * constructor for DTO representing employees. Unlike {@link EmployeeDTO}, this requires a {@link CareEntity} to
     * initialize.
     *
     * @param care
     */
    public EmployeeCareDTO(CareEntity care) {
        this.name = care.getEmployee().getName();
        this.svnr = String.valueOf(care.getEmployee().getSvnr());
        this.email = care.getEmployee().getEmail();
        this.salary = Double.toString(care.getEmployee().getSalary());
        this.employment = StringConversions.properGrammar(care.getEmployee().getEmployment().toString());
        this.gender = StringConversions.properGrammar(care.getEmployee().getGender().toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSvnr() {
        return svnr;
    }

    public void setSvnr(String svnr) {
        this.svnr = svnr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getEmployment() {
        return employment;
    }

    public void setEmployment(String employment) {
        this.employment = employment;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
