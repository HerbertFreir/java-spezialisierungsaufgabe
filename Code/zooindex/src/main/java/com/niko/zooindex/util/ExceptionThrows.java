package com.niko.zooindex.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * is a utility class providing custom-made exception throws
 */
public class ExceptionThrows {
    public static ResponseStatusException employeeNotFound = new ResponseStatusException(HttpStatus.NOT_FOUND, "No employee found.");
    public static ResponseStatusException animalNotFound = new ResponseStatusException(HttpStatus.NOT_FOUND, "No animal found.");
    public static ResponseStatusException careNotFound = new ResponseStatusException(HttpStatus.NOT_FOUND, "No care found.");
    public static ResponseStatusException emptyField = new ResponseStatusException(HttpStatus.NOT_FOUND, "One or more of the required fields is empty.");
    public static ResponseStatusException duplicateSvnr = new ResponseStatusException(HttpStatus.CONFLICT, "SVNR already exists.");
    public static ResponseStatusException duplicateEmail = new ResponseStatusException(HttpStatus.CONFLICT, "Email already exists.");
    public static ResponseStatusException invalidField = new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "The given information is of an invalid format");
}
