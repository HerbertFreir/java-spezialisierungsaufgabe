package com.niko.zooindex.dto;

/**
 * used for frontend communication. Unlike {@link AnimalCareDTO}, this one is used for creating database entries.
 */
public class NewCareDTO {
    private int employeeId;
    private int animalId;

    /**
     * constructor used for posting new {@link com.niko.zooindex.model.CareEntity}s
     *
     * @param employeeId
     * @param animalId
     */
    public NewCareDTO(int employeeId, int animalId) {
        this.employeeId = employeeId;
        this.animalId = animalId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }
}
