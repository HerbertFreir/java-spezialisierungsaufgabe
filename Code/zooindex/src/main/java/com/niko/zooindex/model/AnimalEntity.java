package com.niko.zooindex.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.niko.zooindex.enumeration.AnimalGender;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class AnimalEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String food;

    @Column
    private int age;

    @Enumerated(EnumType.STRING)
    private AnimalGender gender;

    @OneToMany(mappedBy = "animal", cascade = CascadeType.MERGE)
    @JsonManagedReference
    private Set<CareEntity> careSet = new HashSet<CareEntity>();

    @ManyToOne
    @JoinColumn(name = "ClassificationId")
    private ClassificationEntity classification;

    public AnimalEntity(){
    }

    /**
     * Constructor for an animalEntity. This is the format of stored animal data in the database.
     * Note that gender is an ENUM and classification is a class.
     *
     * @param name
     * @param food
     * @param age
     * @param gender
     * @param classification
     */
    public AnimalEntity(String name, String food, int age, AnimalGender gender, ClassificationEntity classification) {
        this.name = name;
        this.food = food;
        this.age = age;
        this.gender = gender;
        this.classification = classification;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public AnimalGender getGender() {
        return gender;
    }

    public void setGender(AnimalGender gender) {
        this.gender = gender;
    }

    public Set<CareEntity> getCareSet() {
        return careSet;
    }

    public void addCare(CareEntity care) {
        this.careSet.add(care);
    }

    public ClassificationEntity getClassification() {
        return classification;
    }

    public void setClassification(ClassificationEntity classification) {
        this.classification = classification;
    }
}
