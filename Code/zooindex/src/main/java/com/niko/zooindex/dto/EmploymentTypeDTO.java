package com.niko.zooindex.dto;

import com.niko.zooindex.enumeration.EmploymentType;
import com.niko.zooindex.util.StringConversions;

import java.util.ArrayList;
import java.util.List;

/**
 * used for frontend communication.
 */
public class EmploymentTypeDTO {
    private String employmentType;

    /**
     * for frontend communication. Converts an {@link EmploymentType} into a String with proper grammar.
     *
     * @param employmentType
     */
    public EmploymentTypeDTO(EmploymentType employmentType){
        this.employmentType = StringConversions.properGrammar(employmentType.toString());
    }
    /**
     * used for compiling a list of available employmentTypes for the frontend.
     *
     * @return ArrayList (String)
     */
    public static List<String> getEmploymentTypes() {

        List<String> employmentTypeList = new ArrayList<>();
        String employmentTypeBefore = "";
        for (EmploymentType employmentType : EmploymentType.values()) {

            employmentTypeBefore = employmentType.toString();
            employmentTypeList.add(StringConversions.properGrammar(employmentTypeBefore));
        }
            return employmentTypeList;
    }

    public String getEmploymentType() { return employmentType; }

    public void setEmploymentType(String employmentType) { this.employmentType = employmentType; }
}