package com.niko.zooindex.controller;

import com.niko.zooindex.dto.EmployeeDTO;
import com.niko.zooindex.dto.EmploymentTypeDTO;
import com.niko.zooindex.dto.SalaryDTO;
import com.niko.zooindex.enumeration.EmployeeGender;
import com.niko.zooindex.enumeration.EmploymentType;
import com.niko.zooindex.enumeration.NumberComparator;
import com.niko.zooindex.enumeration.Workday;
import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.model.EmployeeEntity;
import com.niko.zooindex.repository.CareCRUDRepository;
import com.niko.zooindex.repository.EmployeeCRUDRepository;
import com.niko.zooindex.util.ExceptionThrows;
import com.niko.zooindex.util.StringConversions;
import com.niko.zooindex.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * A controller for {@link EmployeeEntity}s.
 */
@RestController
public class EmployeeController {

    @Autowired
    EmployeeCRUDRepository ecr;
    @Autowired
    CareCRUDRepository ccr;

    /**
     * converts database entries into DTOs and sends the compiled list of employees to the frontend
     *
     * @return ArrayList
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/employees")
    public List<EmployeeDTO> getAllEmployees() {
        List<EmployeeDTO> employeeList = new ArrayList<>();
        Iterable<EmployeeEntity> employeeItr = ecr.findAll();

        for (EmployeeEntity employee : employeeItr) {
            Set<String> days = new HashSet<>();

            for (Workday workday : employee.getWorkdaySet())
                days.add(StringConversions.properGrammar(workday.toString()));

            employeeList.add(new EmployeeDTO(employee.getId(), employee.getName(), String.valueOf(employee.getSvnr()), employee.getEmail(),
                    String.valueOf(employee.getSalary()), StringConversions.properGrammar(employee.getEmployment().toString()),
                    StringConversions.properGrammar(employee.getGender().toString()), days));
        }
        return employeeList;
    }

    /**
     * converts JSON objects from the frontend into an {@link EmployeeEntity} and makes an entry into the database
     *
     * @param employeeDTO
     * @return employeeDTO
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/newEmployee")
    public EmployeeDTO postNewEmployee(@RequestBody EmployeeDTO employeeDTO) {

        Optional<EmployeeEntity> employeeOptional = ecr.findOneByEmail(employeeDTO.getEmail());
        if (employeeOptional.isPresent()) { throw ExceptionThrows.duplicateEmail; }
        employeeOptional = ecr.findOneBySvnr(Long.parseLong(employeeDTO.getSvnr()));
        if (employeeOptional.isPresent()) { throw ExceptionThrows.duplicateSvnr; }

        if (employeeDTO.getName().isEmpty() ||
                employeeDTO.getEmail().isEmpty() ||
                employeeDTO.getEmployment() == null ||
                employeeDTO.getGender() == null ||
                employeeDTO.getSalary() == null ||
                employeeDTO.getSvnr() == null) { throw ExceptionThrows.emptyField; }

        if (!Validation.validateEmail(employeeDTO.getEmail()) ||
                !Validation.validateEmploymentType(employeeDTO.getEmployment()) ||
                !Validation.validateEmployeeGender(employeeDTO.getGender())) { throw ExceptionThrows.invalidField; }

        if (Long.parseLong(employeeDTO.getSvnr()) > 999999999 && Long.parseLong(employeeDTO.getSvnr()) < 10000000000L) {
            try {
                ecr.save(new EmployeeEntity(employeeDTO.getName(), Long.parseLong(employeeDTO.getSvnr()), employeeDTO.getEmail(),
                        Double.parseDouble(employeeDTO.getSalary()), EmploymentType.convertStringToEnum(employeeDTO.getEmployment()),
                        EmployeeGender.convertStringToEnum(employeeDTO.getGender())));
            } catch (Exception exception) {
                throw ExceptionThrows.invalidField;
            }
        }
        return employeeDTO;
    }

    /**
     * converts JSON objects from the frontend into an {@link EmployeeEntity} and updates existing database entries
     *
     * @param employeeDTO
     * @param id
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, path = "/updateEmployee/{id}")
    public void updateEmployee(@RequestBody EmployeeDTO employeeDTO, @PathVariable int id) {
        Optional<EmployeeEntity> optionalEmployee = ecr.findById(id);
        if (optionalEmployee.isEmpty()) { throw ExceptionThrows.employeeNotFound; }
        EmployeeEntity updateEmployee = ecr.findById(id).get();
        if (employeeDTO.getName().isEmpty() ||
                employeeDTO.getEmail().isEmpty() ||
                employeeDTO.getEmployment() == null ||
                employeeDTO.getGender() == null ||
                employeeDTO.getSalary() == null ||
                employeeDTO.getSvnr() == null) { throw ExceptionThrows.emptyField; }
        updateEmployee.setName(employeeDTO.getName());

        if (!Validation.validateEmail(employeeDTO.getEmail()) ||
                !Validation.validateEmploymentType(employeeDTO.getEmployment()) ||
                !Validation.validateEmployeeGender(employeeDTO.getGender())) { throw ExceptionThrows.invalidField; }
        updateEmployee.setEmail(employeeDTO.getEmail());
        updateEmployee.setEmployment(EmploymentType.convertStringToEnum(employeeDTO.getEmployment()));
        updateEmployee.setGender(EmployeeGender.convertStringToEnum(employeeDTO.getGender()));

        try {
            updateEmployee.setSalary(Double.parseDouble(employeeDTO.getSalary()));
        } catch (Exception exception) { throw ExceptionThrows.invalidField; }

        if (employeeDTO.getSvnr().length() == 10) {
            try {
                updateEmployee.setSvnr(Long.parseLong(employeeDTO.getSvnr()));
            } catch (Exception exception) { throw ExceptionThrows.invalidField; }
        } else throw ExceptionThrows.invalidField;
        ecr.save(updateEmployee);
    }

    /**
     * sends a list of all available {@link EmploymentType}s to the frontend.
     *
     * @return ArrayList (String)
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/employmentTypes")
    public List<String> getAllEmploymentTypes() {
        return EmploymentTypeDTO.getEmploymentTypes();
    }

    /**
     * deletes the employee under given ID
     *
     * @param id
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/deleteEmployee/{id}")
    public void deleteEmployee(@PathVariable int id) {
        try {
            for(CareEntity care : ccr.findAllByEmployee(ecr.findById(id).get())){
                ccr.delete(care);
            }
            ecr.deleteById(id);
        } catch (Exception exception) { throw ExceptionThrows.employeeNotFound; }
    }

    /*
    SEARCH AND FILTER REQUESTS ARE BELOW
     */

    /**
     * returns employee under given name. Should the distinct search fail, it will instead search for employees which
     * have the given String in its name.
     *
     * @param name
     * @return ArrayList ({@link EmployeeEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "employeesByName/{name}")
    public List<EmployeeEntity> getEmployeesByName(@PathVariable String name) {
        List<EmployeeEntity> employeeList = new ArrayList<>();
        try {
            EmployeeEntity employee = ecr.findOneByName(name);
            if (employee == null) {
                employeeList.addAll(ecr.findByNameLike("%" + name + "%"));
            }
            else { employeeList.add(employee); }
            return employeeList;
        } catch (Exception exception) { throw ExceptionThrows.employeeNotFound; }
    }

    /**
     * returns employee under given SVNR. As SVNR is unique it will only attempt to return a single entity.
     *
     * @param svnr
     * @return {@link EmployeeEntity}
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "employeeBySVNR/{svnr}")
    public EmployeeEntity getEmployeeBySVNR(@PathVariable Long svnr) {
        EmployeeEntity employee;
        try {
            employee = ecr.findOneBySvnr(svnr).get();
        } catch (Exception exception) { throw ExceptionThrows.employeeNotFound; }
        return employee;
    }

    /**
     * returns employee under given E-Mail. As E-Mails are unique it will only attempt to return a single entity.
     *
     * @param email
     * @return {@link EmployeeEntity}
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "employeeByEmail/{email}")
    public EmployeeEntity getEmployeeByEmail(@PathVariable String email) {
        EmployeeEntity employee;
        try {
            employee = ecr.findOneByEmail(email).get();
        } catch (Exception exception) { throw ExceptionThrows.employeeNotFound; }
        return employee;
    }

    /**
     * returns employees matching the salary amount condition. This method processes a given double value together with
     * a comparator (>, =, <) and searches for all employees fitting concerning their salary.
     *
     * @param salaryDTO
     * @return ArrayList ({@link EmployeeEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "employeesBySalary")
    public List<EmployeeEntity> getEmployeesBySalary(@RequestBody SalaryDTO salaryDTO) {
        List<EmployeeEntity> employeeEntityList = new ArrayList<>();
        try{ Double.parseDouble(salaryDTO.getSalary()); }
        catch (Exception exception){ throw ExceptionThrows.invalidField; }
        if (!Validation.validateNumberComparator(salaryDTO.getNumberComparator())) { throw ExceptionThrows.invalidField; }
        if (NumberComparator.convertStringToEnum(salaryDTO.getNumberComparator()) == NumberComparator.EQUALS) {
            employeeEntityList.addAll(ecr.findBySalary(Double.parseDouble(salaryDTO.getSalary())));
        }
        if (NumberComparator.convertStringToEnum(salaryDTO.getNumberComparator()) == NumberComparator.MORE) {
            employeeEntityList.addAll(ecr.findBySalaryGreaterThan(Double.parseDouble(salaryDTO.getSalary())));
        }
        if (NumberComparator.convertStringToEnum(salaryDTO.getNumberComparator()) == NumberComparator.LESS) {
            employeeEntityList.addAll(ecr.findBySalaryLessThan(Double.parseDouble(salaryDTO.getSalary())));
        }
        return employeeEntityList;
    }

    /**
     * returns employees matching the employmentType
     *
     * @param employmentType
     * @return ArrayList {@link EmployeeEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/employeesByEmploymentType/{employmentType}")
    public List<EmployeeEntity> getEmployeesByEmploymentType(@PathVariable String employmentType) {
        if (employmentType.isEmpty()) { throw ExceptionThrows.emptyField; }
        else if (!Validation.validateEmploymentType(employmentType)) { throw ExceptionThrows.invalidField; }
        return ecr.findByEmployment(EmploymentType.convertStringToEnum(employmentType));
    }

    /**
     * returns employees matching the gender
     *
     * @param gender
     * @return ArrayList ({@link EmployeeEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/employeesByGender/{gender}")
    public List<EmployeeEntity> getEmployeesByGender(@PathVariable String gender){
        if(gender.isEmpty()) { throw ExceptionThrows.emptyField; }
        else if(!Validation.validateEmployeeGender(gender)) { throw ExceptionThrows.invalidField; }
        return ecr.findByGender(EmployeeGender.convertStringToEnum(gender));
    }
}
