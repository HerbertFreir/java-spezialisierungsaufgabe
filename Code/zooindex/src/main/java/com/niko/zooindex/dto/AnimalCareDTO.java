package com.niko.zooindex.dto;

import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.util.StringConversions;

/**
 * used for frontend communication
 */
public class AnimalCareDTO {

    private String speciesType;
    private String animalType;
    private String name;
    private String food;
    private String gender;
    private int age;

    /**
     * used for frontend communication. Unlike {@link NewCareDTO}, this class is used to show the animals' data when
     * displaying, which animals an employee is responsible for.
     *
     * @param care
     */
    public AnimalCareDTO(CareEntity care) {

        this.speciesType = StringConversions.properGrammar(care.getAnimal().getClassification().getAnimalClass().toString());
        this.animalType = care.getAnimal().getClassification().getAnimalCommonName();
        this.name = care.getAnimal().getName();
        this.food = care.getAnimal().getFood();
        this.gender = StringConversions.properGrammar(care.getAnimal().getGender().toString());
        this.age = care.getAnimal().getAge();
    }

    public String getSpeciesType() {
        return speciesType;
    }

    public void setSpeciesType(String speciesType) {
        this.speciesType = speciesType;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

  /* public List<String> getDay() {
        return day;
    }

    public void addDay(String day) {
        this.day.add(day);
    }*/
}
