package com.niko.zooindex.enumeration;

/**
 * lists all genders available for employees
 */
public enum EmployeeGender {
    MALE,
    FEMALE,
    DIVERSE;

    /**
     * used for converting a given String into a corresponding ENUM.
     *
     * @param employeeGender
     * @return EmployeeGender (ENUM)
     */
    public static EmployeeGender convertStringToEnum(String employeeGender){

        String employeeGenderUpper = employeeGender.toUpperCase();
        EmployeeGender employeeGenderConv = EmployeeGender.valueOf(employeeGenderUpper);

        return employeeGenderConv;
    }
}
