package com.niko.zooindex.dto;

/**
 * used for frontend communication
 */
public class AgeDTO {
    private String age;
    private String numberComparator;

    /**
     * constructor for a DTO to filter animals by age
     * @param age
     * @param numberComparator
     */
    public AgeDTO(String age, String numberComparator) {
        this.age = age;
        this.numberComparator = numberComparator;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getNumberComparator() {
        return numberComparator;
    }

    public void setNumberComparator(String numberComparator) {
        this.numberComparator = numberComparator;
    }
}
