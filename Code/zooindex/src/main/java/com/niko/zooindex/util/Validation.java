package com.niko.zooindex.util;

import com.niko.zooindex.enumeration.AnimalGender;
import com.niko.zooindex.enumeration.EmployeeGender;
import com.niko.zooindex.enumeration.EmploymentType;
import com.niko.zooindex.enumeration.NumberComparator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * is a utility class containing custom-made methods validating information received from the frontend
 */
public class Validation {

    private static boolean isValid = false;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * uses a regular expression to check if the entered E-Mail is of a valid format
     *
     * @see Pattern
     * @param emailStr
     * @return boolean
     */
    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    /**
     * checks if the given String is one of the available {@link EmploymentType}s in the zoo
     *
     * @param employmentTypeDTO
     * @return boolean
     */
    public static boolean validateEmploymentType(String employmentTypeDTO) {
        isValid = false;
        for (EmploymentType employmentType : EmploymentType.values()) {
            if(StringConversions.properGrammar(employmentType.toString()).equals(StringConversions.properGrammar(employmentTypeDTO))){
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    /**
     * checks if the given String is one of the available {@link EmployeeGender}s for employees
     *
     * @param employeeGenderDTO
     * @return boolean
     */
    public static boolean validateEmployeeGender(String employeeGenderDTO){
        isValid = false;
        for(EmployeeGender employeeGender : EmployeeGender.values()){
            if(StringConversions.properGrammar(employeeGender.toString()).equals(StringConversions.properGrammar(employeeGenderDTO))){
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    /**
     * checks if the given String is one of the available {@link AnimalGender}s for animals
     *
     * @param animalGenderDTO
     * @return boolean
     */
    public static boolean validateAnimalGender(String animalGenderDTO){
        isValid = false;
        for(AnimalGender animalGender : AnimalGender.values()){
            if(StringConversions.properGrammar(animalGender.toString()).equals(StringConversions.properGrammar(animalGenderDTO))){
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    /**
     * checks if the given String is one of the available {@link NumberComparator}s allowed to run
     *
     * @param numComp
     * @return boolean
     */
    public static boolean validateNumberComparator(String numComp) {
        isValid = false;
        for (NumberComparator numberComparators : NumberComparator.values()) {
            if(StringConversions.properGrammar(numberComparators.toString()).equals(StringConversions.properGrammar(numComp))){
                isValid = true;
                break;
            }
        }
        return isValid;
    }
}
