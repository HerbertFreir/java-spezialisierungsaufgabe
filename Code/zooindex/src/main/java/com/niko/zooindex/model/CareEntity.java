package com.niko.zooindex.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class CareEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int careId;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "employeeId")
    private EmployeeEntity employee;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "animalId")
    private AnimalEntity animal;

    public CareEntity(){
    }

    /**
     * Constructor for careEntity. a careEntity is made up of its primary key(careId) and two(2)
     * objects, namely an employee(via "employeeId") and an animal(via "animalId").
     * @param employee
     * @param animal
     * @see EmployeeEntity
     * @see AnimalEntity
     */
    public CareEntity(EmployeeEntity employee, AnimalEntity animal) {
        this.employee = employee;
        this.animal = animal;
    }

    public int getCareId() {
        return careId;
    }

    public void setCareId(int careId) {
        this.careId = careId;
    }

    public EmployeeEntity getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeEntity employee) {
        this.employee = employee;
    }

    public AnimalEntity getAnimal() {
        return animal;
    }

    public void setAnimal(AnimalEntity animal) {
        this.animal = animal;
    }
}
