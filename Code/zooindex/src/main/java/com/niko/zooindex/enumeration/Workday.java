package com.niko.zooindex.enumeration;

/**
 * ENUM class for all weekdays
 */
public enum Workday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
