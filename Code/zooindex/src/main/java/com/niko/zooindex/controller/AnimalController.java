package com.niko.zooindex.controller;

import com.niko.zooindex.dto.AgeDTO;
import com.niko.zooindex.dto.AnimalDTO;
import com.niko.zooindex.dto.NewAnimalDTO;
import com.niko.zooindex.enumeration.AnimalGender;
import com.niko.zooindex.enumeration.NumberComparator;
import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.model.ClassificationEntity;
import com.niko.zooindex.repository.AnimalCRUDRepository;
import com.niko.zooindex.repository.AnimalClassificationCRUDRepository;
import com.niko.zooindex.repository.CareCRUDRepository;
import com.niko.zooindex.util.ExceptionThrows;
import com.niko.zooindex.util.StringConversions;
import com.niko.zooindex.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A controller for {@link AnimalEntity}s.
 */
@RestController
public class AnimalController {

    @Autowired
    AnimalCRUDRepository acr;
    @Autowired
    AnimalClassificationCRUDRepository accr;
    @Autowired
    CareCRUDRepository ccr;

    /**
     * returns a list of all animals in the database
     *
     * @return ArrayList (String)
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animals")
    public List<AnimalDTO> getAllAnimals(){
       List<AnimalDTO> animalDTOList = new ArrayList<>();
       Iterable<AnimalEntity> animalItr = acr.findAll();

       for(AnimalEntity animal:animalItr){
           animalDTOList.add(new AnimalDTO(animal.getId(), animal.getName(), animal.getFood(),
                   animal.getAge(), StringConversions.properGrammar(animal.getGender().toString()), animal.getClassification()));
       }
        return animalDTOList;
    }

    /**
     * returns a list of all classifications used to reduce redundancy
     *
     * @return ArrayList ({@link ClassificationEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalTypes")
    public List<String> getAllAnimalClassifications(){
        List<String> classificationList = new ArrayList<>();
        Iterable<ClassificationEntity> classificationIterable = accr.findAll();
        for(ClassificationEntity classification : classificationIterable){
            classificationList.add(classification.getAnimalCommonName());
        }
        return classificationList;
    }

    /**
     * converts JSON objects from the frontend into {@link AnimalEntity}s and adds them to the database
     *
     * @param newAnimalDTO
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/newAnimal")
    public AnimalEntity addNewAnimal(@RequestBody NewAnimalDTO newAnimalDTO) {
        ClassificationEntity tempClassification = new ClassificationEntity();
        try {
            for (ClassificationEntity classification : accr.findAll()) {
                if (newAnimalDTO.getSpeciesType().equals(classification.getAnimalCommonName())) {
                    tempClassification = classification;
                    break;
                }
            }
        } catch (Exception exception) { throw ExceptionThrows.invalidField; }
        if (newAnimalDTO.getName().isEmpty() ||
                newAnimalDTO.getFood().isEmpty() ||
                Integer.toString(newAnimalDTO.getAge()) == null ||
                newAnimalDTO.getGender().isEmpty() ||
                tempClassification.getAnimalCommonName() == null) { throw ExceptionThrows.emptyField; }

        return acr.save(new AnimalEntity(newAnimalDTO.getName(), newAnimalDTO.getFood(), newAnimalDTO.getAge(),
                AnimalGender.convertStringToEnum(newAnimalDTO.getGender()), tempClassification));
    }

    /**
     * converts JSON objects from the frontend into an {@link AnimalEntity} and updates existing database entries
     *
     * @param newAnimalDTO
     * @param id
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, path = "/updateAnimal/{id}")
    public void updateAnimal(@RequestBody NewAnimalDTO newAnimalDTO, @PathVariable int id){
        Optional<AnimalEntity> optionalAnimal = acr.findById(id);
        if(optionalAnimal.isEmpty()){ throw ExceptionThrows.animalNotFound; }
        AnimalEntity updateAnimal = acr.findById(id).get();
        if(newAnimalDTO.getName().isEmpty() ||
        Integer.valueOf(newAnimalDTO.getAge()) == null ||
        newAnimalDTO.getFood().isEmpty() ||
        newAnimalDTO.getGender().isEmpty()){ throw ExceptionThrows.emptyField; }
        if(!newAnimalDTO.getSpeciesType().isEmpty()){
            try {
                for (ClassificationEntity classificationEntity : accr.findAll()) {
                    if (newAnimalDTO.getSpeciesType().equals(classificationEntity.getAnimalCommonName())) {
                        updateAnimal.setClassification(classificationEntity);
                        break;
                    }
                }
        } catch (Exception exception) { throw ExceptionThrows.invalidField; }
        }
        updateAnimal.setName(newAnimalDTO.getName());
        updateAnimal.setAge(newAnimalDTO.getAge());
        updateAnimal.setFood(newAnimalDTO.getFood());
        updateAnimal.setGender(AnimalGender.convertStringToEnum(newAnimalDTO.getGender()));
        acr.save(updateAnimal);
    }

    /**
     * deletes the animal under the given ID
     *
     * @param id
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/deleteAnimal/{id}")
    public void deleteAnimal(@PathVariable int id) {
        try {
            for(CareEntity care : ccr.findAllByAnimal(acr.findById(id).get())){
                ccr.delete(care);
            }
            acr.deleteById(id);
        } catch (Exception exception) { throw ExceptionThrows.animalNotFound; }
    }
        /*
    SEARCH AND FILTER REQUESTS ARE BELOW
     */

    /**
     * returns animal under given name. Should the distinct search fail, it will instead search for animals which
     * have the given String in its name.
     *
     * @param name
     * @return ArrayList ({@link AnimalEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByName/{name}")
    public List<AnimalEntity> getAnimalsByName(@PathVariable String name){
        List<AnimalEntity> animalList = new ArrayList<>();
        try {
            AnimalEntity animal = acr.findOneByName(name);
            if (animal == null) {
                animalList.addAll(acr.findByNameLike("%" + name + "%"));
            }
            else { animalList.add(animal); }
            return animalList;
        } catch (Exception exception) { throw ExceptionThrows.animalNotFound; }
    }

    /**
     * returns animals eating given food.
     *
     * @param food
     * @return ArrayList ({@link AnimalEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByFood/{food}")
    public List<AnimalEntity> getAnimalsByFood(@PathVariable String food){
        List<AnimalEntity> animalList = new ArrayList<>();
        animalList.addAll(acr.findByFood(food));
        if(animalList.isEmpty()){ throw ExceptionThrows.animalNotFound; }

        return animalList;
    }

    /**
     * returns animals matching the age. This method processes a given Integer value together with
     * a comparator (>, =, <) and searches for all animals fitting concerning their age.
     *
     * @param ageDTO
     * @return ArrayList ({@link AnimalEntity})
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByAge/")
    public List<AnimalEntity> getAnimalsByAge(@RequestBody AgeDTO ageDTO){
        List<AnimalEntity> animalList = new ArrayList<>();
        try{ Integer.parseInt(ageDTO.getAge()); }
        catch (Exception exception){ throw ExceptionThrows.invalidField; }
        if (!Validation.validateNumberComparator(ageDTO.getNumberComparator())) { throw ExceptionThrows.invalidField; }
        if (NumberComparator.convertStringToEnum(ageDTO.getNumberComparator()) == NumberComparator.EQUALS) {
            animalList.addAll(acr.findByAge(Integer.parseInt(ageDTO.getAge())));
        }
        if (NumberComparator.convertStringToEnum(ageDTO.getNumberComparator()) == NumberComparator.MORE) {
            animalList.addAll(acr.findByAgeGreaterThan(Integer.parseInt(ageDTO.getAge())));
        }
        if (NumberComparator.convertStringToEnum(ageDTO.getNumberComparator()) == NumberComparator.LESS) {
            animalList.addAll(acr.findByAgeLessThan(Integer.parseInt(ageDTO.getAge())));
        }
        return animalList;
    }

    /**
     * returns animals matching the gender
     *
     * @param gender
     * @return
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByGender/{gender}")
    public List<AnimalEntity> getAnimalsByGender(@PathVariable String gender){
        if(!Validation.validateAnimalGender(gender)){ throw ExceptionThrows.invalidField; }
        List<AnimalEntity> animalList = new ArrayList<>();
        animalList.addAll(acr.findByGender(AnimalGender.convertStringToEnum(gender)));
        if (animalList.isEmpty()){ throw ExceptionThrows.animalNotFound; }
        return animalList;
    }

    /**
     * returns animals matching the classification
     *
     * @param classification
     * @return
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByClassification/{classification}")
    public List<AnimalEntity> getAnimalsByClassification(@PathVariable String classification){
        List<AnimalEntity> animalList = new ArrayList<>();
        try {
            for (ClassificationEntity classificationEntity : accr.findAll()) {
                if (classification.equals(classificationEntity.getAnimalCommonName())) {
                    animalList.addAll(acr.findByClassification(classificationEntity));
                    break;
                }
            }
        }
        catch (Exception exception){ throw ExceptionThrows.invalidField; }
        return animalList;
    }
}
