package com.niko.zooindex.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.niko.zooindex.enumeration.EmployeeGender;
import com.niko.zooindex.enumeration.EmploymentType;
import com.niko.zooindex.enumeration.Workday;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column(unique = true)
    private long svnr;

    @Column(unique = true)
    private String email;

    @Column
    private double salary;

    @Enumerated(EnumType.STRING)
    private EmploymentType employment;

    @Enumerated(EnumType.STRING)
    private EmployeeGender gender;

    @ElementCollection
    private Set<Workday> workdaySet = new HashSet<Workday>();

    @OneToMany(mappedBy = "employee", cascade = CascadeType.MERGE)
    @JsonManagedReference
    private Set<CareEntity> careSet = new HashSet<CareEntity>();

    public EmployeeEntity(){
    }

    /**
     * constructor for EmployeeEntity. This is the format of stored employee data in the database. Note that gender and
     * employmentType are ENUMs.
     *
     * @param name
     * @param svnr
     * @param email
     * @param salary
     * @param employmentType
     * @param gender
     */
    public EmployeeEntity(String name, long svnr, String email, double salary, EmploymentType employmentType, EmployeeGender gender) {
        this.name = name;
        this.svnr = svnr;
        this.email = email;
        this.salary = salary;
        this.employment = employmentType;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSvnr() {
        return svnr;
    }

    public void setSvnr(long svnr) {
        this.svnr = svnr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public EmploymentType getEmployment() {
        return employment;
    }

    public void setEmployment(EmploymentType employment) {
        this.employment = employment;
    }

    public Set<Workday> getWorkdaySet() { return workdaySet; }

    public void addWorkday(Workday workday) { workdaySet.add(workday); }

    public EmployeeGender getGender() {
        return gender;
    }

    public void setGender(EmployeeGender gender) {
        this.gender = gender;
    }

    public Set<CareEntity> getCareSet() {
        return careSet;
    }

    public void addCare(CareEntity care) {
        this.careSet.add(care);
    }
}
