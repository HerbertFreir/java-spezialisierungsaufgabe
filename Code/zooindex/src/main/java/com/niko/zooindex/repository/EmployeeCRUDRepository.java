package com.niko.zooindex.repository;

import com.niko.zooindex.enumeration.EmployeeGender;
import com.niko.zooindex.enumeration.EmploymentType;
import com.niko.zooindex.model.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeCRUDRepository extends CrudRepository<EmployeeEntity, Integer> {
    EmployeeEntity findOneByName(String name);
    List<EmployeeEntity> findByNameLike(String name);
    Optional<EmployeeEntity> findOneBySvnr(long svnr);
    Optional<EmployeeEntity> findOneByEmail(String email);
    List<EmployeeEntity> findBySalary(double salary);
    List<EmployeeEntity> findBySalaryGreaterThan(double salary);
    List<EmployeeEntity> findBySalaryLessThan(double salary);
    List<EmployeeEntity> findByEmployment(EmploymentType employmentType);
    List<EmployeeEntity> findByGender(EmployeeGender employeeGender);
}
