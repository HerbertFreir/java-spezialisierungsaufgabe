package com.niko.zooindex.controller;

import com.niko.zooindex.dto.AnimalCareDTO;
import com.niko.zooindex.dto.EmployeeCareDTO;
import com.niko.zooindex.dto.NewCareDTO;
import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.model.EmployeeEntity;
import com.niko.zooindex.repository.AnimalCRUDRepository;
import com.niko.zooindex.repository.CareCRUDRepository;
import com.niko.zooindex.repository.CareRepository;
import com.niko.zooindex.repository.EmployeeCRUDRepository;
import com.niko.zooindex.util.ExceptionThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A controller for {@link CareEntity}s.
 */
@RestController
public class CareController {

    @Autowired
    AnimalCRUDRepository acr;
    @Autowired
    EmployeeCRUDRepository ecr;
    @Autowired
    CareCRUDRepository ccr;
    @Autowired
    CareRepository cr;

    /**
     * returns a list of all {@link AnimalEntity}s assigned to the given {@link EmployeeEntity}
     *
     * @param id
     * @return
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/animalsByEmployee/{id}")
    public List<AnimalCareDTO> getCaresByEmployee(@PathVariable int id){
        Optional<EmployeeEntity> employeeOptional = ecr.findById(id);
        if (employeeOptional.isEmpty()) { throw ExceptionThrows.employeeNotFound; }
        EmployeeEntity employee = employeeOptional.get();
        List<AnimalCareDTO> careList = new ArrayList<>();
        for(CareEntity care : ccr.findAllByEmployee(employee)){
            careList.add(new AnimalCareDTO(care));
        }
    return careList;
    }

    /**
     * returns a list of all {@link EmployeeEntity}s assigned to the given {@link AnimalEntity}
     *
     * @param id
     * @return
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/employeesByAnimal/{id}")
    public List<EmployeeCareDTO> getCaresByAnimal(@PathVariable int id){
        Optional<AnimalEntity> animalOptional = acr.findById(id);
        if(animalOptional.isEmpty()){ throw ExceptionThrows.animalNotFound; }
        AnimalEntity animal = animalOptional.get();
        List<EmployeeCareDTO> careList = new ArrayList<>();
        for(CareEntity care : ccr.findAllByAnimal(animal)){
            careList.add(new EmployeeCareDTO(care));
        }
        return careList;
    }

    /**
    creates a new {@link CareEntity} and adds it to the database
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/newCare")
    public void addNewCare(@RequestBody NewCareDTO newCareDTO){
        Optional<EmployeeEntity> optionalEmployee = ecr.findById(newCareDTO.getEmployeeId());
        if(optionalEmployee.isEmpty()){throw ExceptionThrows.employeeNotFound;}
        Optional<AnimalEntity> optionalAnimal = acr.findById(newCareDTO.getAnimalId());
        if(optionalAnimal.isEmpty()){throw ExceptionThrows.animalNotFound;}

        cr.setCare(newCareDTO.getEmployeeId(), newCareDTO.getAnimalId());
    }

    /**
     * removes a {@link CareEntity} from the database, the affected animal and the employee
     *
     * @param id
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/deleteCare/{id}")
    public void deleteCare(@PathVariable int id){
        try{
            ccr.findById(id).get().getAnimal().getCareSet().remove(ccr.findById(id));
            ccr.findById(id).get().getEmployee().getCareSet().remove(ccr.findById(id));
            ccr.delete(ccr.findById(id).get());
        }
        catch (Exception exception){ throw ExceptionThrows.careNotFound; }
    }
}
