package com.niko.zooindex.enumeration;

/**
 * lists all genders animals are known to have
 */
public enum AnimalGender {
    MALE,
    FEMALE,
    HERMAPHRODITE,
    NONE;

    /**
     * used for converting a given String into a corresponding ENUM.
     *
     * @param animalGender
     * @return AnimalGender (ENUM)
     */
    public static AnimalGender convertStringToEnum(String animalGender){

        String animalGenderUpper = animalGender.toUpperCase();
        AnimalGender animalGenderConv = AnimalGender.valueOf(animalGenderUpper);

        return animalGenderConv;
    }
}
