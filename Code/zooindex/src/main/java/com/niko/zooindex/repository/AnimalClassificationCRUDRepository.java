package com.niko.zooindex.repository;

import com.niko.zooindex.model.ClassificationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimalClassificationCRUDRepository extends CrudRepository<ClassificationEntity, Integer> {
}
