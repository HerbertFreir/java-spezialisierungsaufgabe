package com.niko.zooindex.repository;

import com.niko.zooindex.model.AnimalEntity;
import com.niko.zooindex.model.CareEntity;
import com.niko.zooindex.model.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CareCRUDRepository extends CrudRepository<CareEntity, Integer> {
    List<CareEntity> findAllByEmployee(EmployeeEntity employee);
    List<CareEntity> findAllByAnimal(AnimalEntity animal);
}
