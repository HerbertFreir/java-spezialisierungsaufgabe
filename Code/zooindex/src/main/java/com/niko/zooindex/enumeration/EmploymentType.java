package com.niko.zooindex.enumeration;

/**
 * represents all the different professions which came to mind when thinking about who works in a zoo.
 */
public enum EmploymentType {
    GUIDE,
    ADMINISTRATOR,
    PERSONELL_ADMINISTRATOR,
    ANIMAL_EXPERT,
    CLEANER,
    HANDLER,
    SECURITY,
    VET,
    SALESPERSON,
    LOGISTICIAN,
    TECHNICIAN,
    GARDENER,
    CLIMBER,
    LEGAL_TEAM;

    /**
     * used for converting a given String into a corresponding ENUM.
     *
     * @param employmentType
     * @return EmploymentType (ENUM)
     */
    public static EmploymentType convertStringToEnum(String employmentType){

        String employmentTypeUpperString = employmentType.toUpperCase();
        EmploymentType employmentTypeConv = EmploymentType.valueOf(employmentTypeUpperString);

        return employmentTypeConv;
    }
}
