package com.niko.zooindex.model;

import com.niko.zooindex.enumeration.AnimalClass;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * AnimalClassification is a class assigned to an animal. It is composed of the animal's common name (for example:
 * house cat, grey wolf, ...), the animalGenus which represents the broader definition and animalSpecies which
 * represents the more exact variant of a Genus-Type and animalDescription which paints a better picture of the animal.
 * The scientific animal name is composed of a combination of animalGenus and animalSpecies.
 */
@Entity
public class ClassificationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String animalCommonName;

    @Column
    private String animalGenus;

    @Column
    private String animalSpecies;

    @Column
    private String animalDescription;

    @Enumerated(EnumType.STRING)
    private AnimalClass animalClass;

    @OneToMany(mappedBy = "id")
    private Set<AnimalEntity> describedAnimal = new HashSet<>();

    public ClassificationEntity(){
    }

    /**
     * used to reduce redundancy when creating animals. A classificationEntity can be assigned to
     * {@link AnimalEntity} instances.
     * @param animalCommonName
     * @param animalGenus
     * @param animalSpecies
     * @param animalDescription
     * @param animalClass
     */
    public ClassificationEntity(String animalCommonName, String animalGenus, String animalSpecies, String animalDescription, AnimalClass animalClass) {
        this.animalCommonName = animalCommonName;
        this.animalGenus = animalGenus;
        this.animalSpecies = animalSpecies;
        this.animalDescription = animalDescription;
        this.animalClass = animalClass;
    }

    public int getId() {
        return id;
    }

    public String getAnimalCommonName() {
        return animalCommonName;
    }

    public void setAnimalCommonName(String animalCommonName) {
        this.animalCommonName = animalCommonName;
    }

    public String getAnimalGenus() {
        return animalGenus;
    }

    public void setAnimalGenus(String animalGenus) {
        this.animalGenus = animalGenus;
    }

    public String getAnimalSpecies() {
        return animalSpecies;
    }

    public void setAnimalSpecies(String animalSpecies) {
        this.animalSpecies = animalSpecies;
    }

    public String getAnimalDescription() {
        return animalDescription;
    }

    public void setAnimalDescription(String animalDescription) {
        this.animalDescription = animalDescription;
    }

    public AnimalClass getAnimalClass() {
        return animalClass;
    }

    public void setAnimalClass(AnimalClass animalClass) {
        this.animalClass = animalClass;
    }

    public Set<AnimalEntity> getDescribedAnimal() {
        return describedAnimal;
    }

    public void addDescribedAnimal(AnimalEntity animal) {
        this.describedAnimal.add(animal);
    }
}
