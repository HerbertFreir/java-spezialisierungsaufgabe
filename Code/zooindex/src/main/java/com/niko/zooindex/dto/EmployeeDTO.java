package com.niko.zooindex.dto;

import java.util.HashSet;
import java.util.Set;

/**
 * used for frontend communication.
 */
public class EmployeeDTO {

    private Set<String> days = new HashSet<>();
    private int id;
    private String name;
    private String svnr;
    private String email;
    private String salary;
    private String employment;
    private String gender;

    /**
     * constructor for DTO representing an {@link com.niko.zooindex.model.EmployeeEntity}. All parameters' datatypes
     * have been replaced with Strings except id.
     *
     * @param id
     * @param name
     * @param svnr
     * @param email
     * @param salary
     * @param employment
     * @param gender
     * @param days
     */
    public EmployeeDTO(int id, String name, String svnr, String email, String salary, String employment, String gender, Set<String> days) {
        this.id = id;
        this.name = name;
        this.svnr = svnr;
        this.email = email;
        this.salary = salary;
        this.employment = employment;
        this.gender = gender;
        this.days = days;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSvnr() { return svnr; }

    public void setSvnr(String svnr) {
        this.svnr = svnr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalary() { return salary; }

    public void setSalary(String salary) { this.salary = salary; }

    public String getEmployment() {
        return employment;
    }

    public void setEmployment(String employment) {
        this.employment = employment;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Set<String> getDays() {
        return days;
    }

    public void setDays(Set<String> days) {
        this.days = days;
    }
}
