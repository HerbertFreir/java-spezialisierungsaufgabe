package com.niko.zooindex.dto;

/**
 * used for frontend communication. NewAnimalDTO provides less information in its package than {@link AnimalDTO}.
 */
public class NewAnimalDTO {

    private int id;
    private String name;
    private String food;
    private int age;
    private String gender;
    private String speciesType;

    /**
     * constructor used for adding new {@link com.niko.zooindex.model.AnimalEntity}s to the database
     *
     * @param id
     * @param name
     * @param food
     * @param age
     * @param gender
     * @param speciesType
     */
    public NewAnimalDTO(int id, String name, String food, int age, String gender, String speciesType) {
        this.id = id;
        this.name = name;
        this.food = food;
        this.age = age;
        this.gender = gender;
        this.speciesType = speciesType;
    }

    public int getId() {
        return id;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSpeciesType() {
        return speciesType;
    }

    public void setSpeciesType(String speciesType) {
        this.speciesType = speciesType;
    }
}